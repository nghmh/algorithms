import queue
#import sets
class Node:
    def __init__(self, id):
        self.id = id
    def __repr__(self):
        return self.__str__()
    def __str__(self):
        return f"Node({self.id})"

#edge_list is a tuple of nodes [(Node(id),Node(id)),...,(Node(id),Node(id))]
def build_adjacency_list(edge_list):
    adj_list = {}
    for edge in edge_list:
        if edge[0] in adj_list:
            adj_list[edge[0]].append(edge[1])
        else:
            adj_list[edge[0]] = [edge[1]]
    return adj_list

def bfs(adj_list, s):
    q = queue.Queue(maxsize=0)
    visited_node = set()
    predecessor = {s:Node(-1)}
    q.put(s) # [0]
    while q.empty()==False:
        current_node = q.get() # q: [3], v: [0,1]
        if current_node in visited_node:
            continue
        print(current_node)
        visited_node.add(current_node) # q: [], v: [0, 1, 2]
        for neighbour in adj_list[current_node]:
            if neighbour not in visited_node:
                q.put(neighbour)  # q: [3, 3], v: [0, 1, 2]
            if neighbour not in predecessor:
                predecessor[neighbour] = current_node
    print(predecessor)


if __name__ == '__main__':
    #test
    nodes = [Node(0), Node(1), Node(2), Node(3)]
    edges = [(nodes[0], nodes[1]),
             (nodes[1], nodes[2]),
             (nodes[1], nodes[3]),
             (nodes[2], nodes[3]),
             (nodes[3], nodes[1]),
             (nodes[3], nodes[0])]
    adj_list = build_adjacency_list(edges)
    bfs(adj_list, nodes[0])
